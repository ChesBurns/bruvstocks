import requests
import json
from createStock import newStock
from dbManage import updateStocks

# Stock list class
class Stock_list:
    list = []

    # Builds stock list from portfolioData txt file
    def create(self):
        self.list = []
        file = open('portfolioData.txt')
        for line in file:
            for word in line.split():
                self.list.append(newStock(word))

    # Adds to user's portfolio
    def add(self, stock, user):
        inPortfolio = False
        portfolio_data = open('portfolioData.txt')
        for line in portfolio_data:
            for word in line.split():
                if stock.symbol.upper() == word:
                    inPortfolio = True

        if not inPortfolio:
            with open('portfolioData.txt', 'a') as file:
                    file.write(stock.symbol.upper() + "\n")
            self.list.append(stock)
            print('LIST:', self.list)

        updateStocks(user)

    def clear(self):
        open('portfolioData.txt', 'w').close()
        self.list = []

    def get(self):
        return self.list

    # Deletes desired stock from portfolio
    def delete(self, stock_to_remove, user):
        old_stockList = self.list
        for stock in self.list:
            if stock.symbol.upper() == stock_to_remove.upper():
                self.list.remove(stock)

        open('portfolioData.txt', 'w').close()

        with open('portfolioData.txt', 'a') as file:
            for stock in self.list:
                file.write(stock.symbol.upper() + "\n")

        updateStocks(user)

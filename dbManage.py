import sqlite3


# Prepares list for SQL entry
def stringify(list):
    stringList = ''
    for item in list:
        stringList += (item + ',')

    return stringList[:-1]

# Creates table, dev tool only, not used in normal use
def createTable():
    c.execute("""CREATE TABLE users (
                user TEXT PRIMARY KEY,
                stocks TEXT
                )""")


# Creates a new user in the sql database
def newUser(username, pswd):
    import sqlite3
    import password
    conn = sqlite3.connect('userDatabase.db')
    c = conn.cursor()

    c.execute("""INSERT INTO users VALUES(
                '{u}',
                ''
                )""".format(u=username))
    conn.commit()
    c.close()
    conn.close()

    password.store(username, pswd)


# Sets the portfolio data to that of the logged in user
def loginUser(username):
    import sqlite3
    conn = sqlite3.connect('userDatabase.db')
    c = conn.cursor()

    c.execute("SELECT stocks FROM users WHERE user = '{}'".format(username))
    stocks = str(c.fetchall()[0][0])
    stocks = stocks.split(',')

    conn.commit()
    c.close()
    conn.close()

    open('portfolioData.txt', 'w').close()

    for code in stocks:
        add = True
        p = open('portfolioData.txt')
        for line in p:
            for word in line.split():
                if code.upper() == word:
                    add = False

        with open('portfolioData.txt', 'a') as file:
            if add:
                file.write(code.upper() + "\n")


# Updates the stocks of the user
def updateStocks(user):
    import sqlite3
    conn = sqlite3.connect('userDatabase.db')
    c = conn.cursor()

    file = open('portfolioData.txt')
    codes = []
    for line in file:
        for code in line.split():
            codes.append(code)

    stocks = stringify(codes)
    c.execute("UPDATE users SET stocks = '{s}' WHERE user = '{u}'".format(s=stocks, u=user))

    conn.commit()
    c.close()
    conn.close()

# Tests to see if a user exists in the database
def inUserList(username):
    import sqlite3
    conn = sqlite3.connect('userDatabase.db')
    c = conn.cursor()
    c.execute("SELECT user from users")
    users = [user[0] for user in c.fetchall()]

    if username in users:
        return True
    else:
        return False

    conn.commit()
    c.close()
    conn.close()

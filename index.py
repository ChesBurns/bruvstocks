import requests
import json

# Gets the growth of a stock's exchange index
def getGrowth(exchange):
    if exchange == 'Nasdaq Global Select':
        symbol = 'IXIC'
    elif exchange == 'New York Stock Exchange':
        symbol = 'NYA'
    else:
        symbol = 'IXIC'

    link = "https://www.alphavantage.co/query?function=TIME_SERIES_MONTHLY&symbol=" + symbol + "&apikey=GMJTBU1KGH7YS8QT"
    price_data = requests.get(link, verify=False).json()
    price_data = price_data["Monthly Time Series"]
    keys = list(price_data.keys())
    closeCurrent = float(price_data[keys[0]]['4. close'])
    closePrevious_day = float(price_data[keys[1]]['4. close'])
    growth_day = (closeCurrent - closePrevious_day) / closePrevious_day
    growth_day = growth_day * 100

    for date, prices in price_data.items():
        if date[:4] == str(int(keys[0][:4]) - 1):
            closePrevious_year = prices['4. close']
            break

    growth_year = (closeCurrent - float(closePrevious_year)) / float(closePrevious_year)

    return {"growth_day": float(growth_day), "growth_year": float(growth_year)}

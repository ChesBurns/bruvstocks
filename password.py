from cryptography.fernet import Fernet

# Stores new password
def store(user, password):
    cipherSuite = Fernet(b'U3uSuhhabrIUu8EohWLnjerEEcEUdw3vTOhSPJojQ6g=')
    cipheredText = cipherSuite.encrypt(password.encode())
    with open('.bin/{}.bin'.format(user), 'wb') as file:
        file.write(cipheredText)

# Verifies password
def verify(user, password):
    cipherSuite = Fernet(b'U3uSuhhabrIUu8EohWLnjerEEcEUdw3vTOhSPJojQ6g=')
    with open('.bin/{}.bin'.format(user), 'rb') as file:
        for line in file:
            pswrd = line

    decryptedPassword = bytes(cipherSuite.decrypt(pswrd)).decode('utf-8')
    if decryptedPassword == password:
        return True
    else:
        return False

import requests
import json
from stock import Stock


# newStock takes the desired stock's code and retrieves all the necessary datapoints
def newStock(code):
    quoteLink = str("https://api.iextrading.com/1.0/stock/" + code + "/quote?displayPercent=true")
    quote = requests.get(quoteLink, verify=False).json()

    dividendLink = str("https://api.iextrading.com/1.0/stock/" + code + '/dividends/5y')
    dividend = requests.get(dividendLink, verify=False).json()

    companyLink = str("https://api.iextrading.com/1.0/stock/" + code + "/company")
    company = requests.get(companyLink, verify=False).json()

    stock = Stock()
    stock.profileLink = "http://localhost:5000/loadStock?stock="+code
    stock.updateTime = quote['latestTime'].upper()
    stock.symbol = code.upper()
    stock.name = company['companyName']
    stock.description = company['description']
    stock.industry = company['industry']
    stock.sector = company['sector']
    stock.ceo = company['CEO']
    stock.exchange = company['exchange']
    stock.website = company['website']
    stock.price = quote['latestPrice']
    stock.change = float("%.2f" % quote['change'])
    stock.changePercent = float("%.2f" % quote['changePercent'])
    stock.prevClose = quote['previousClose']
    stock.open = quote['open']
    stock.high = quote['high']
    stock.low = quote['low']
    stock.yearHigh = quote['week52High']
    stock.yearLow = quote['week52Low']
    stock.volume = format(quote['latestVolume'], ",")
    stock.avgVolume = format(quote['avgTotalVolume'], ",")
    stock.mktCap = format(quote['marketCap'], ",")
    stock.peRatio = quote['peRatio']
    try:
        stock.divYield = dividend[0]['amount']

    except IndexError:
        stock.divYield = 'N/A'
    stock.ytdChange = quote['ytdChange']

    if company['exchange'] == 'Nasdaq Global Select':
        stock.chartLink = "https://www.tradingview.com/symbols/NASDAQ-" + code.upper() + "/"
        stock.chartSym = "NASDAQ:" + code.upper()
    elif company['exchange'] == 'New York Stock Exchange':
        stock.chartLink = "https://www.tradingview.com/symbols/NYSE-" + code.upper() + "/"
        stock.chartSym = "NYSE:" + code.upper()
    elif company['exchange'] == 'NYSE American':
        stock.chartLink = "https://www.tradingview.com/symbols/AMEX-" + code.upper() + "/"
        stock.chartSym = "AMEX:" + code.upper()

    return stock

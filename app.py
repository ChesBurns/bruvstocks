from flask import Flask, render_template, request, redirect, url_for
from portfolio import Stock_list
from recommend import Recommend_list
from createStock import newStock
from inPortfolio import stockInPortfolio
from password import verify
import requests
from dbManage import inUserList, updateStocks, loginUser, newUser
from testStock import testStock
import time
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


# Temporary stock object for stockProfile use
class Temp_stock:
    tempStockObj = ''

    def changeTemp(self, stock):
        self.tempStockObj = stock

    def getTemp(self):
        return self.tempStockObj

# User class
class User:
    username = ''

    def updateUser(self, newUsername):
        self.username = newUsername

    def getUser(self):
        return self.username


open('portfolioData.txt', 'w').close()  # clearing portfolioData file
app = Flask(__name__)
recommendList = Recommend_list()
stockProf = Temp_stock()
stockList = Stock_list()
currentUser = User()

app.jinja_env.tests['inStockList'] = stockInPortfolio
app.jinja_env.globals.update(user=currentUser.getUser())
app.jinja_env.globals.update(loggedIn=False)
recommendList.create()

# Homepage
@app.route('/')
def index():
    app.jinja_env.globals.update(user=currentUser.getUser())
    return render_template('home.html')

# Login page
@app.route('/login', methods=['GET', 'POST'])
def login():
    open('portfolioData.txt', 'w').close()
    err = None
    if request.method == 'POST':
        # Testing if credentials entered are valid
        if inUserList(request.form['username']) is False or verify(request.form['username'], request.form['password']) is False:
            err = 'Invalid Credentials'
        else:

            # Logs in the user
            loginUser(request.form['username'])
            app.jinja_env.globals.update(loggedIn=True)
            currentUser.updateUser(request.form['username'])
            app.jinja_env.globals.update(user=currentUser.getUser())
            stockList.create()
            return redirect(url_for('portfolio'))

    return render_template('login.html', error=err)

# Log out
@app.route('/logout')
def logout():
    currentUser.updateUser('')
    stockList.clear()
    app.jinja_env.globals.update(loggedIn=False)
    return redirect(url_for('index'))

# Sign up new user
@app.route('/signup', methods=['GET', 'POST'])
def signUp():
    open('portfolioData.txt', 'w').close()
    err = None
    if request.method == 'POST':
        if inUserList(request.form['username']):
            err = 'Username Taken'
        else:
            # Creates new user attributes
            newUser(request.form['username'], request.form['password'])
            currentUser.updateUser(request.form['username'])
            stockList.create()
            app.jinja_env.globals.update(loggedIn=True)
            loginUser(request.form['username'])
            app.jinja_env.globals.update(user=currentUser.getUser())
            return redirect(url_for('portfolio'))

    return render_template('signup.html', error=err)

# Portfolio
@app.route('/portfolio')
def portfolio():
    portfolioData = stockList.get()

    return render_template('portfolio.html', ptflo=portfolioData)


# Add stocks
@app.route('/addstocks', methods=['GET', 'POST'])
def addstocks():
    day = True
    rankMethod = request.args.get('rank')

    if rankMethod == 'compGrowth_year':
        day = False
    elif rankMethod == 'compGrowth_day':
        day = True
    elif rankMethod is None:
        rankMethod = 'compGrowth_day'
        day = True

    error = ''
    if request.method == 'POST':
        url = '/loadStock?stock=' + request.form['stockSearch']
        if testStock(request.form['stockSearch']):
            return redirect(url)

        else:
            error = 'Invalid symbol'
            return render_template('addstocks.html', rcmd=recommendList.rank(rankMethod), err=error, d=day)

    return render_template('addstocks.html', rcmd=recommendList.rank(rankMethod), err=error, d=day)


# About
@app.route('/about')
def about():
    return render_template('about.html')


# Stock profile
@app.route('/stockProfile')
def stockprofile():
    destStock = request.args.get('stock')
    if stockInPortfolio(destStock):
        for s in stockList.get():
            if s.symbol == destStock:
                stockProf.changeTemp(s)
                break

    else:
        stockProf.changeTemp(newStock(destStock))

    return render_template('stockProfile.html', stk=stockProf.getTemp())


@app.route('/add')
def add():
    stockList.add(stockProf.getTemp(), currentUser.getUser())
    return redirect(url_for('portfolio'))


# Load stock screen
@app.route('/loadStock')
def loadScreen():
    stockCodePass = request.args.get('stock')
    return render_template('loadStock.html', stkCode=stockCodePass)

# Help
@app.route('/help')
def help():
    return render_template('help.html')


# Remove stock
@app.route('/remove')
def remove():
    stock_to_remove = request.args.get('stock')
    stockList.delete(stock_to_remove, currentUser.getUser())
    return redirect(url_for('portfolio'))


if __name__ == '__main__':
    app.run(debug=True)

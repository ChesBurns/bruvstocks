# Tests if inputted symbol is an accessible stock
import requests
import json

def testStock(code):

    quoteLink = str("https://api.iextrading.com/1.0/stock/" + code + "/quote")
    try:
        quote = requests.get(quoteLink, verify=False).json()

    except:
        return False

    return True

import requests
import json
from createStock import newStock
from index import getGrowth
from operator import itemgetter

# Class for recommended stocks list
class Recommend_list():

    stocksCompGrowth = []
    stocks = []

    # Builds the list from the txt file
    def create(self):
        file = open('recommendation.txt')

        for line in file:
            for code in line.split():
                self.stocks.append(newStock(code))

        nasdaqGrowth = getGrowth('Nasdaq Global Select')
        nyseGrowth = getGrowth('New York Stock Exchange')

        stocksCompGrowth = []

        for x in self.stocks:
            compareDict = {}
            if x.exchange == 'Nasdaq Global Select':
                compareDict['symbol'] = x.symbol
                compareDict['compGrowth_day'] = (x.changePercent/100) / nasdaqGrowth['growth_day']
                compareDict['compGrowth_year'] = (x.ytdChange/100) / nasdaqGrowth['growth_year']

            elif x.exchange == 'New York Stock Exchange':
                compareDict['symbol'] = x.symbol
                compareDict['compGrowth_day'] = (x.changePercent/100) / nyseGrowth['growth_day']
                compareDict['compGrowth_year'] = (x.ytdChange/100) / nyseGrowth['growth_year']

            self.stocksCompGrowth.append(compareDict)

    # Ranks the index growths by the desired attribute 'sortBy'
    def rank(self, sortBy):
        rankedStocks = sorted(self.stocksCompGrowth, key=itemgetter(sortBy))

        rankedStocks_list = []
        rankNum = 1
        for item in rankedStocks:
            for stock in self.stocks:
                if stock.symbol == item['symbol']:
                    stock.rank = rankNum
                    rankedStocks_list.append(stock)
                    rankNum += 1

        rankedStocks_list.sort(key=lambda x: x.rank)

        return rankedStocks_list

# Tests to see if a given stock exists in the portfolio
def stockInPortfolio(code):
    symbolList = []
    with open('portfolioData.txt', 'r') as file:
        for stock in file:
            symbolList.append(stock[:-1])

    print(symbolList)
    if code.upper() in symbolList:
        return True
    else:
        return False
